#include <string>
#include <stdexcept>
#include "nucleotide_count.h"

using namespace std;

namespace dna {
  counter::counter(string in_string) {
    for (auto c = in_string.begin();
	 c < in_string.end();
	 c++) {
      if (*c != 'A' && *c != 'C' && *c != 'G' && *c != 'T') {
	throw invalid_argument("Bleh");
      }
    }
    s = in_string;
  }

  int counter::count(char nucleotide) const {
    bool is_nuc = false;
    for (int i = 0; i < 4; i++) {
      if (nucleotide == nucleotides[i]) {
	is_nuc = true;
      }
    }
    if (!is_nuc) {
      throw invalid_argument("Not a valid nucleotide");
    }
    int ctr = 0;
    for (auto c = s.begin(); c < s.end(); c++) {
      ctr += (*c == nucleotide);
    }
    return ctr;
  }

  map<char, int> counter::nucleotide_counts() const {
    auto counts = new map<char, int>();
    int cA = this->count('A');
    int cC = this->count('C');
    int cT = this->count('T');
    int cG = this->count('G');
    counts->insert(make_pair('A', cA));
    counts->insert(make_pair('C', cC));
    counts->insert(make_pair('T', cT));
    counts->insert(make_pair('G', cG));
    return map<char, int>(*counts);
  }
}
