#if !defined(NUCLEOTIDE_COUNT_H)
#define NUCLEOTIDE_COUNT_H

#include <string>
#include <map>

using namespace std;

namespace dna{
  class counter {
  public:
    string s;
    const char nucleotides[4]{'A', 'C', 'T', 'G'};
    counter(string g);
    int count(char nucleotide) const;
    map<char, int> nucleotide_counts() const;
  };
}
#endif
